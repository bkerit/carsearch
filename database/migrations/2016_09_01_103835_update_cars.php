<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */static
    public function up()
    {
        //
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('bodyworks');
            $table->integer('brand');
            $table->integer('color');
            $table->integer('country');
            $table->integer('doors');
            $table->integer('entry');
            $table->integer('extras');
            $table->integer('family');
            $table->integer('freetime');
            $table->integer('fuel');
            $table->integer('gear');
            $table->integer('livingcar');
            $table->integer('plaque');
            $table->integer('seats');
            $table->integer('sport');
            $table->integer('status');
            $table->integer('trailer_hitch');
            $table->integer('type');
            $table->integer('work');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
