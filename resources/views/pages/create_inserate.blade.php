@extends('layouts.app')
@section('content')

        <!doctype html>
<head>
</head>

<body>
    <h4>Bitte Formular ausfüllen</h4>
    <form method="post">
        <select name="brand">
            @foreach($property->brands as $brand)
                <option value="{{$brand->id}}">{{$brand->brand}}</option>
                @endforeach
        </select>
        <?php $counter = 0; ?>
        <fieldset class="form-inline">
        @foreach($property->bodywork as $bodyworks)
        <input type="checkbox" id="bodyworks{{$counter}}" name="bodyworks{{$counter}}" value="{{$bodyworks->id}}">
        <label for="bodyworks{{$counter}}">{{$bodyworks->wert}}</label>
        <?php $counter ++; ?>
        @endforeach
        </fieldset>
        <select name="color">
            @foreach($property->colors as $color)
                <option value="{{$color->id}}">{{$color->color}}</option>
                @endforeach
        </select>
        <select name="country">
            @foreach($property->countrys as $country)
                <option value="{{$country->id}}">{{$country->country_id}}</option>
                @endforeach
        </select>
        <select name="doors">
            @foreach($property->door as $doors)
                <option value="{{$doors->id}}">{{$doors->doors}}</option>
                @endforeach
        </select>
        <select name="entry">
            @foreach($property->entrys as $entry)
                <option value="{{$entry->id}}">{{$entry->entry}}</option>
                @endforeach
        </select>
        <select name="extras">
            @foreach($property->extra as $extras)
                <option value="{{$extras->id}}">{{$extras->extra}}</option>
                @endforeach
        </select>
        <select name="fuel">
            @foreach($property->fuels as $fuel)
                <option value="{{$fuel->id}}">{{$fuel->fuel}}</option>
            @endforeach
        </select>
        <select name="gear">
            @foreach($property->gears as $gear)
                <option value="{{$gear->id}}">{{$gear->gear}}</option>
            @endforeach
        </select>
        <select name="plaque">
            @foreach($property->plaques as $plaque)
                <option value="{{$plaque->id}}">{{$plaque->plaque}}</option>
            @endforeach
        </select>
        <select name="seats">
            @foreach($property->seats as $seats)
                <option value="{{$seats->id}}">{{$seats->seats}}</option>
            @endforeach
        </select>
        <select name="status">
            @foreach($property->statuse as $status)
                <option value="{{$status->id}}">{{$status->status}}</option>
            @endforeach
        </select>
        <select name="trailer_hitch">
            @foreach($property->trailer_hitches as $trailer_hitch)
                <option value="{{$trailer_hitch->id}}">{{$trailer_hitch->trailer_hitch}}</option>
            @endforeach
        </select>
        <select name="type">
            @foreach($property->types as $type)
                <option value="{{$type->id}}">{{$type->type}}</option>
            @endforeach
        </select>
    </form>
</body>
    @stop