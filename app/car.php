<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class car extends Model
{
    //
    public function color(){
        return $this->hasOne('App\color');
    }

    public function bodywork(){
        return $this->hasMany('App\bodywork');
    }

    public function brand(){
        return $this->hasOne('App\brand');
    }

    public function country(){
        return $this->hasOne('App\country');
    }

    public function doors(){
        return $this->hasOne('App\doors');
    }

    public function entry(){
        return $this->hasOne('App\entry');
    }
    public function extras(){
        return $this->hasMany('App\extras');
    }

    public function family(){
        return $this->hasOne('App\family');
    }

    public function freetime(){
        return $this->hasOne('App\freetime');
    }

    public function fuel(){
        return $this->hasOne('App\fuel');
    }

    public function gear(){
        return $this->hasOne('App\gear');
    }

    public function livingcar(){
        return $this->hasOne('App\livingcar');
    }

    public function plaque(){
        return $this->hasOne('App\plaque');
    }

    public function seats(){
        return $this->hasOne('App\seats');
    }

    public function sport(){
        return $this->hasOne('App\sport');
    }

    public function status(){
        return $this->hasOne('App\status');
    }

    public function trailer_hitch(){
        return $this->hasOne('App\trailer_hitch');
    }

    public function type(){
        return $this->hasOne('App\type');
    }

    public function work(){
        return $this->hasOne('App\work');
    }


}
