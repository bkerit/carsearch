<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brand extends Model
{
    //

    public function models()
    {
        return $this->hasMany('App\models', 'brands_id', 'id');
    }
}
