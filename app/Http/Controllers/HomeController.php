<?php

namespace App\Http\Controllers;

use App\extras;
use Illuminate\Http\Request;
use App\bodywork;
use App\brand;
use App\color;
use App\country;
use App\doors;
use App\entry;
use App\family;
use App\freetime;
use App\fuel;
use App\gear;
use App\livingcar;
use App\plaque;
use App\seats;
use App\sport;
use App\status;
use App\trailer_hitch;
use App\type;
use App\work;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public $car;
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->car->bodyworks = bodywork::all();
        $this->car->brands = brand::all();
        $this->car->colors = color::all();
        $this->car->countrys = country::all();
        $this->car->door = doors::all();
        $this->car->entrys = entry::all();
        $this->car->extra = extras::all();
        $this->car->familys = family::all();
        $this->car->freetimes = freetime::all();
        $this->car->fuels = fuel::all();
        $this->car->gears = gear::all();
        $this->car->livingcars = livingcar::all();
        $this->car->plaques = plaque::all();
        $this->car->seat = seats::all();
        $this->car->sports = sport::all();
        $this->car->statuse = status::all();
        $this->car->trailer_hitches = trailer_hitch::all();
        $this->car->types = type::all();
        $this->car->works = work::all();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function createInserate(){
        return view('pages.create_inserate')
            ->with('property', $this->car);
    }
}
